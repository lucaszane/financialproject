using System;
using AutoMapper;
using Financial.Domain.Users;
using Financial.Domain.Users.Models;

namespace Financial.Api.Configurations.Mappings.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<CreateUser, User>()
                .ConstructUsing(command =>
                {
                    var user = new User(
                        id: command.Id,
                        name: command.Name,
                        password: command.Password,
                        email: command.Email,
                        userName: command.UserName
                    );

                    return user;
                });

            CreateMap<UpdateUser, User>()
                .ConvertUsing((command, user) =>
                {
                    user.Update(
                        name: command.Name,
                        password: command.Password,
                        email: command.Email,
                        userName: command.UserName
                    );

                    return user;
                });
        }

    }
}