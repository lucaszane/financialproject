using AutoMapper;
using Financial.Api.Configurations.Mappings.Profiles;

namespace Financial.Api.Configurations.Mappings
{
    public class AutoMapperConfiguration
    {
        #region Public Static Methods

        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(profile => { profile.AddProfile(new UserProfile()); });
        }

        #endregion
    }
}