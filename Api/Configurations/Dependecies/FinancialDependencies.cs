using System;
using Microsoft.Extensions.DependencyInjection;
using Financial.Infra.Context;
using Financial.Infra.Repositories;
using Financial.Domain.Users.Repositories;

namespace Financial.Api.Configurations.Dependencies
{
    public static class FinancialDependencies
    {
        public static IServiceCollection AddFinancialDependencies(this IServiceCollection services)
        {

            // Repositories

            services.AddScoped<IUserRepository, UserRepository>();


            // Handlers

            // services.AddScoped<IRequestHandler<CreateFinancial, Guid?>, FinancialCommandsHandler>();

            // Events

            // services.AddScoped<INotificationHandler<FinancialCreated>, FinancialEventsHandler>();

            return services;
        }
    }
}
