using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Financial.Domain.Users.Models;
using Financial.Domain.Users.Repositories;
using Microsoft.AspNetCore.Mvc;
using Financial.Domain.Users;

namespace financial.api.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UsersController : ControllerBase
    {

        private readonly IUserRepository _usersRepository;
        private readonly IMapper _mapper;
        public UsersController(
            IUserRepository usersRepository,
            IMapper mapper)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] CreateUser user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            var result = _mapper.Map<User>(user);
            _usersRepository.AddNewUser(result);

            return Ok(result);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
