using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Financial.Domain.Users;
using Financial.Domain.Users.Repositories;
using Financial.Domain.Users.Models;
using Financial.Infra.Context;
using System.Threading.Tasks;

namespace Financial.Infra.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly FinancialContext _context;
        private readonly DbSet<User> _users;

        public UserRepository(FinancialContext context)
        {
            _context = context;
            _users = context.Users;
        }

        public async Task<UserItem> GetById(Guid id)
        {
            var result = await _users
                .Where(x => x.Id.Equals(id))
                .Select(x => new UserItem
                {
                    Id = x.Id
                }).FirstOrDefaultAsync();

            return result;
        }

        public async Task<UserItem> Authenticate(string login, string password)
        {
            var result = await _users
                .Where(x => x.UserName.Equals(login) && x.Password.Equals(password))
                .Select(x => new UserItem
                {
                    UserName = x.UserName,
                    Name = x.Name
                }).FirstOrDefaultAsync();

            return result;
        }

        public void AddNewUser(User user)
        {
            _users.Add(user);
        }

    }
}
