using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Financial.Infra.Context
{
    public class UnitOfWork
    {
        private readonly FinancialContext _financialContext;

        public UnitOfWork(FinancialContext FinancialContext)
        {
            _financialContext = FinancialContext;
        }

        public async Task<bool> CommitAsync()
        {
            var rowsAffected = await _financialContext.SaveChangesAsync();
            return rowsAffected > 0;
        }
    }
}
