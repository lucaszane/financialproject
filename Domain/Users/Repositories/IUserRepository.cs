using Financial.Domain.Users.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Financial.Domain.Users.Repositories
{
    public interface IUserRepository
    {
        Task<UserItem> GetById(Guid id);
        Task<UserItem> Authenticate(string login, string password);
        void AddNewUser(User user);
    }
}
