using System;

namespace Financial.Domain.Users
{
    public class User
    {
        public User()
        { }


        public User(Guid id, string userName, string password, string email, string name)
        {
            Id = id;
            Name = name;
            Password = password;
            UserName = userName;
            Email = email;
        }

        public void Update(string userName, string password, string email, string name)
        {
            Name = name;
            Password = password;
            UserName = userName;
            Email = email;
        }

        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
