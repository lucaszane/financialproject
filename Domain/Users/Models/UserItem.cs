using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Domain.Users.Models
{
    public class UserItem
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }

        public string Email { get; set; }
    }
}
