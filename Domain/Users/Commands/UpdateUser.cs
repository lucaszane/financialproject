using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Domain.Users.Models
{
    public class UpdateUser
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
