using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Domain.Users.Models
{
    public class CreateUser
    {
        public Guid Id = new Guid();
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
