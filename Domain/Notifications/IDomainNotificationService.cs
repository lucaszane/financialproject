﻿using System.Collections.Generic;

namespace Financial.Domain.Notifications
{
    public interface IDomainNotificationService
    {
        #region Public Methods

        /// <summary>
        ///     Verifica se existe alguma notificação
        /// </summary>
        /// <returns>Valor booleano (true) se existir notificações ou (false) caso contrário</returns>
        bool HasNotifications();

        /// <summary>
        ///     Retorna as notificações geradas
        /// </summary>
        /// <returns>Lista genérica de notificações</returns>
        List<DomainNotification> GetNotifications();

        /// <summary>
        ///     Adiciona uma notificação de domínio
        /// </summary>
        /// <param name="notification">Objeto de notificação de domínio</param>
        void AddNotification(DomainNotification notification);

        #endregion
    }
}