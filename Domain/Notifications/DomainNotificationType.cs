﻿namespace Financial.Domain.Notifications
{
    public enum DomainNotificationType : short
    {
        UnprocessableEntity = 1,
        ConcurrencyException = 2
    }
}
