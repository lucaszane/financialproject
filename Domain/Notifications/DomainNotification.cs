﻿using System;

namespace Financial.Domain.Notifications
{
    public class DomainNotification
    {
        #region Constructors

        public DomainNotification(string key, string value)
        {
        }

        public DomainNotification(string key, string value, DomainNotificationType type)
        {
            DomainNotificationId = Guid.NewGuid();
            Key = key;
            Value = value;
            Type = type;
        }

        #endregion

        #region Properties

        public Guid DomainNotificationId { get; }
        public string Key { get; }
        public string Value { get; }
        public DomainNotificationType Type { get; set; }

        #endregion
    }
}